-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 26, 2017 at 12:56 AM
-- Server version: 5.5.54-0ubuntu0.14.04.1
-- PHP Version: 5.6.30-10+deb.sury.org~trusty+2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `proje_takip`
--

-- --------------------------------------------------------

--
-- Table structure for table `ayarlar`
--

CREATE TABLE IF NOT EXISTS `ayarlar` (
  `ayar_id` int(11) NOT NULL AUTO_INCREMENT,
  `site_baslik` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_hakkimizda` text COLLATE utf8_unicode_ci NOT NULL,
  `site_iletisim_eposta` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_anasayfa_logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ayar_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ayarlar`
--

INSERT INTO `ayarlar` (`ayar_id`, `site_baslik`, `site_hakkimizda`, `site_iletisim_eposta`, `site_logo`, `site_anasayfa_logo`) VALUES
(1, 'Proje Takibim', 'Bu site Celal Bayar Üniversitesi - Kırkağaç Meslek Yüksekokulu öğrencileri Yusuf Kaya ve Tayfun Serin tarafından bitirme projesi olarak hazırlanmıştır.', 'info@projetakibim.com', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `dosyalar`
--

CREATE TABLE IF NOT EXISTS `dosyalar` (
  `dosya_id` int(11) NOT NULL AUTO_INCREMENT,
  `proje_id` int(11) NOT NULL,
  `dosya_ad` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dosya_uzantı` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dosya_yol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dosya_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dosya_boyut` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`dosya_id`),
  KEY `proje_id` (`proje_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `dosyalar`
--

INSERT INTO `dosyalar` (`dosya_id`, `proje_id`, `dosya_ad`, `dosya_uzantı`, `dosya_yol`, `dosya_link`, `dosya_boyut`) VALUES
(4, 6, '2017-04-20 02.38.26.zip', 'application/zip', '/var/www/html/proje/dosyalar/5 - test/2017-04-20 02.38.26.zip', 'http://localhost/proje/dosyalar/5 - test/2017-04-20 02.38.26.zip', '13640');

-- --------------------------------------------------------

--
-- Table structure for table `kontrol`
--

CREATE TABLE IF NOT EXISTS `kontrol` (
  `kontrol_id` int(11) NOT NULL AUTO_INCREMENT,
  `kontrol_not` text COLLATE utf8_unicode_ci NOT NULL,
  `proje_yuzde` smallint(6) NOT NULL,
  `kontrol_tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `proje_id` int(11) NOT NULL,
  PRIMARY KEY (`kontrol_id`),
  KEY `proje_id` (`proje_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mesajlar`
--

CREATE TABLE IF NOT EXISTS `mesajlar` (
  `mesaj_id` int(11) NOT NULL AUTO_INCREMENT,
  `gonderen_id` int(11) NOT NULL,
  `alici_id` int(11) NOT NULL,
  `mesaj` text COLLATE utf8_unicode_ci NOT NULL,
  `baslik` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `okuma` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`mesaj_id`),
  KEY `gonderen_id` (`gonderen_id`,`alici_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ogrenciler`
--

CREATE TABLE IF NOT EXISTS `ogrenciler` (
  `ogrenci_id` int(11) NOT NULL AUTO_INCREMENT,
  `proje_id` int(11) DEFAULT NULL,
  `ogrenci_no` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ogrenci_sifre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ogrenci_isim` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `ogrenci_eposta` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ogrenci_kayit` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `ogrenci_giris` timestamp NULL DEFAULT NULL,
  `resim` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ogrenci_id`),
  UNIQUE KEY `ogrenci_no` (`ogrenci_no`,`ogrenci_eposta`),
  KEY `proje_id` (`proje_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `ogrenciler`
--

INSERT INTO `ogrenciler` (`ogrenci_id`, `proje_id`, `ogrenci_no`, `ogrenci_sifre`, `ogrenci_isim`, `ogrenci_eposta`, `ogrenci_kayit`, `ogrenci_giris`, `resim`) VALUES
(5, 6, '151809080', 'b45b69c3584350cf9a757654022e73df', 'Yusuf Kaya', 'ysf.ky_1903@hotmail.com', '2017-04-19 21:54:55', '2017-04-25 14:05:50', 'http://localhost/proje/frontend/avatar/1492643485.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `ogretmenler`
--

CREATE TABLE IF NOT EXISTS `ogretmenler` (
  `ogretmen_id` int(11) NOT NULL AUTO_INCREMENT,
  `ogretmen_eposta` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ogretmen_kullaniciadi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ogretmen_ad` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ogretmen_soyad` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ogretmen_sifre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ogretmen_giris` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ogretmen_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ogretmenler`
--

INSERT INTO `ogretmenler` (`ogretmen_id`, `ogretmen_eposta`, `ogretmen_kullaniciadi`, `ogretmen_ad`, `ogretmen_soyad`, `ogretmen_sifre`, `ogretmen_giris`) VALUES
(1, 'admin@admin', 'admin', 'Yusuf', 'Kaya', '21232f297a57a5a743894a0e4a801fc3', '2017-04-19 23:19:49');

-- --------------------------------------------------------

--
-- Table structure for table `olaylar`
--

CREATE TABLE IF NOT EXISTS `olaylar` (
  `olay_id` int(11) NOT NULL AUTO_INCREMENT,
  `proje_id` int(11) DEFAULT NULL,
  `ogrenci_id` int(11) DEFAULT NULL,
  `mesaj_id` int(11) DEFAULT NULL,
  `dosya_id` int(11) DEFAULT NULL,
  `olay` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `olay_tip` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `olay_tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`olay_id`),
  KEY `proje_id` (`proje_id`,`ogrenci_id`,`mesaj_id`,`dosya_id`),
  KEY `proje_id_2` (`proje_id`),
  KEY `ogrenci_id` (`ogrenci_id`),
  KEY `mesaj_id` (`mesaj_id`),
  KEY `dosya_id` (`dosya_id`),
  KEY `proje_id_3` (`proje_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `olaylar`
--

INSERT INTO `olaylar` (`olay_id`, `proje_id`, `ogrenci_id`, `mesaj_id`, `dosya_id`, `olay`, `olay_tip`, `olay_tarih`) VALUES
(1, NULL, 5, NULL, NULL, 'Giriş Yapıldı', 'giriş', '2017-04-25 14:05:50'),
(2, 6, 5, NULL, NULL, 'Proje Güncellendi', 'proje-güncellendi', '2017-04-25 14:06:30');

-- --------------------------------------------------------

--
-- Table structure for table `projeler`
--

CREATE TABLE IF NOT EXISTS `projeler` (
  `proje_id` int(11) NOT NULL AUTO_INCREMENT,
  `proje_no` int(11) DEFAULT NULL,
  `proje_konu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `proje_amac` text COLLATE utf8_unicode_ci NOT NULL,
  `proje_tur` tinyint(4) NOT NULL,
  `olusturan_id` int(11) DEFAULT NULL,
  `proje_dosya` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `proje_uygunluk` tinyint(1) DEFAULT NULL,
  `proje_olusturma` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `proje_bitirme` timestamp NULL DEFAULT NULL,
  `proje_duzenleme` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`proje_id`),
  UNIQUE KEY `proje_no` (`proje_no`),
  KEY `olusturan_id` (`olusturan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `projeler`
--

INSERT INTO `projeler` (`proje_id`, `proje_no`, `proje_konu`, `proje_amac`, `proje_tur`, `olusturan_id`, `proje_dosya`, `proje_uygunluk`, `proje_olusturma`, `proje_bitirme`, `proje_duzenleme`) VALUES
(6, 1, 'test', 'test', 2, 5, '/var/www/html/proje/dosyalar/5 - test', 1, '2017-04-19 23:03:35', '2017-04-19 23:14:35', '2017-04-19 23:15:52');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ogrenciler`
--
ALTER TABLE `ogrenciler`
  ADD CONSTRAINT `foreign_ogrenciler_proje_id_projeler_proje_id` FOREIGN KEY (`proje_id`) REFERENCES `projeler` (`proje_id`) ON DELETE SET NULL;

--
-- Constraints for table `olaylar`
--
ALTER TABLE `olaylar`
  ADD CONSTRAINT `foreign_dosyalar_dosya_id` FOREIGN KEY (`dosya_id`) REFERENCES `dosyalar` (`dosya_id`) ON DELETE SET NULL,
  ADD CONSTRAINT `foreign_mesajlar_mesaj_id` FOREIGN KEY (`mesaj_id`) REFERENCES `mesajlar` (`mesaj_id`) ON DELETE SET NULL,
  ADD CONSTRAINT `foreign_ogrenciler_ogrenci_id` FOREIGN KEY (`ogrenci_id`) REFERENCES `ogrenciler` (`ogrenci_id`) ON DELETE SET NULL,
  ADD CONSTRAINT `foreign_projeler_proje_id` FOREIGN KEY (`proje_id`) REFERENCES `projeler` (`proje_id`) ON DELETE SET NULL;

--
-- Constraints for table `projeler`
--
ALTER TABLE `projeler`
  ADD CONSTRAINT `foreign_ogrenciler_ogrenci_id_projeler_olusturan_id` FOREIGN KEY (`olusturan_id`) REFERENCES `ogrenciler` (`ogrenci_id`) ON DELETE SET NULL;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
